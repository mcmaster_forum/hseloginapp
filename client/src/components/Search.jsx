import React from 'react';
import PropTypes from 'prop-types';
import { Card, CardTitle, CardText } from 'material-ui/Card';

const Search = ({ secretData, user }) => (
  <Card className="container">
    <CardTitle
      title="HSE Search"
      subtitle="You havce been authenticated.  You can now search this site."
    />
  {secretData && <CardText style={{ fontSize: '15px', color: 'blue' }}>Hello <strong>{user.name}</strong>!<br />{secretData}</CardText>}
  </Card>
);

Search.propTypes = {
  secretData: PropTypes.string.isRequired
};

export default Search;
