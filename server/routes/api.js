const express = require('express');

const router = new express.Router();

router.get('/dashboard', (req, res) => {
  res.status(200).json({
    message: "You are cleared to see this page. ",
    // user values passed through from auth middleware
    user: req.user
  });
});




router.get('/search', (req, res) => {
  res.status(200).json({
    message: "You are cleared to see this page. ",
    // user values passed through from auth middleware
    user: req.user
  });
});
router.get('/about', (req, res) => {
  res.status(200).json({
    message: "Welcome to the HSE Search page. ",
    // user values passed through from auth middleware
    user: req.user
  });
});

module.exports = router;
